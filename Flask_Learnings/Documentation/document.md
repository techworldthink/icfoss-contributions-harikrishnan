# Welcome Screen using Flask
***
* At first Flask module was installed by using the command      
// pip install Flask
* created a folder named Project and created a pythonfile -app.py and also created a subfolder named Templates for rendering the html file (index.html).
* In the pythonfile app.py,imported the flask module,named a variable named 'app' and routed for the root url using the command // @app.route('/').Hence when a user acces the root url home function is called.
* Defined a function "home" and returned it into index.html using render template.
* Runned the application using the command:          
// if (__name__)= '__main__': 
       \n app.run=(debug=True)
