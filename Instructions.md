# Task 1
*** 
* Create a folder named "Flask_Learning".
* Create a simple flask application that has a simple homepage (just a welcome screen).
* Document the procedure and add the code and documentation inside the "Flask_Learning" folder.

# Task 2
***
* Develop a Flask application that includes user authentication, login, signup, and redirection to the home page upon successful login.
