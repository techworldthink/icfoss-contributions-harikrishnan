from flask import Flask, render_template
# imported the flask module

app = Flask(__name__)


@app.route('/')
# defined a root url with ('/') using route command ,thus when a user acess the root url it will called into hari function.

def hari():
    return render_template ('index.html')

# rendered to index.html by returning the function by using render_template 


if __name__=='__main__':   # to run the python file as the main program
    app.run(debug=True)    #  starts the Flask development server. The debug=True argument is used to run the server in debug mode,


